<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    
    public function regAct(Request $request){
        $fname = $request['firstname'];
        $lname = $request['lastname'];
        return view('halaman.welcome', compact('fname', 'lname'));
    }
}
