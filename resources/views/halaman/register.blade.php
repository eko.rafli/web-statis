@extends('layout.master')
@section('judul')
    Buat Account Baru
@endsection
@section('title')
Daftar
@endsection
@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname"> First Name:</label><br>
        <input type="text" name="firstname" id="firstname"><br><br>
        <label for="lastname">Last Name:</label><br>
        <input type="text" name="lastname" id="lastname"><br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" name="gender" id="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">indonesia</option>
            <option value="Wkwkland">Wkwkland</option>
            <option value="wakanda">Wakanda</option>
        </select><br><br>
        <label for="languange">Languange Spoken:</label><br>
        <input type="checkbox" name="languange" id="lang-id" value="lang-id">
        <label for="lang-id">Bahasa Indonesia</label><br>
        <input type="checkbox" name="languange" id="lang-en" value="lang-en">
        <label for="lang-en">English</label><br>
        <input type="checkbox" name="languange" id="lang-oth" value="lang-oth">
        <label for="lang-oth">Other</label><br><br>
        <label for="biograph">Bio:</label><br>
        <textarea name="biograph" id="biograph" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
@endsection
