@extends('layout.master')
@section('judul')
    Buat Pemeran
@endsection
@section('title')
Edit Pemeran
@endsection
@section('content')
<h2>Edit Data</h2>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{$cast->nama}}">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur" value="{{$cast->umur}}">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Biodata">{{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/cast" class="btn btn-danger my-1">Batal</a>
</form>
@endsection