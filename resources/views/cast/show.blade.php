@extends('layout.master')
@section('judul')
    Biodata Pemeran {{$cast->id}}
@endsection
@section('title')
Biodata
@endsection
@section('content')
    <h4>{{$cast->nama}}</h4>
    <h5>{{$cast->umur}}</h5>
    <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-danger my-1">Kembali</a>
@endsection