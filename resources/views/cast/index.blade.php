@extends('layout.master')
@section('judul')
Pemeran    
@endsection
@section('title')
Pemeran
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@section('content')
    <a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
    <table class="table table-bordered table-striped">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>
                    <td>
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Tampilkan</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/cast/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Hapus">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>Tidak ada data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection