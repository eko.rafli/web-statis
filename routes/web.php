<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::post('/welcome', 'AuthController@regAct');
Route::get('/register', 'AuthController@register');
Route::get('/data-table', function(){
    return view('table.data-table');
});
Route::get('/table', function(){
    return view('table.table');
});
/*Route::get('/cast', 'CastController@index'); //ok
Route::get('/cast/create', 'CastController@create'); //ok
Route::post('/cast', 'CastController@store'); //ok
Route::get('/cast/{cast_id}', 'CastController@show'); //ok
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //ok
Route::put('/cast/{cast_id}', 'CastController@update'); //ok
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //ok*/
Route::resource('/cast', CastController::class);